package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText usernameInputText;
    private EditText passwordInputText;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        usernameInputText = findViewById(R.id.inputUsername);
        passwordInputText = findViewById(R.id.inputPassword);
        loginButton = findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameInputText.getText().toString();
                String password = passwordInputText.getText().toString();

                if (isValid(username, password)) {
                    Intent intent = new Intent(MainActivity.this, UserProfileActivity.class);
                    intent.putExtra("username", username);
                    intent.putExtra("password", password);
                    startActivity(intent);
                }
            }
        });
    }

    private boolean isValid(String username, String password) {
        return !username.isEmpty() && !password.isEmpty();
    }
}